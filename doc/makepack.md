# make pack

python setup.py sdist是一个用于构建Python软件包的命令。它使用setup.py文件中的配置信息来定义软件包的元数据和依赖关系，并将软件包打包成一个源代码分发包（source distribution package）。

具体而言，sdist是setuptools库提供的一个命令，它执行以下操作：

* 解析setup.py文件：setup.py文件是一个Python脚本，其中包含了定义软件包元数据和依赖关系的代码。sdist命令会解析该文件，获取相关信息。

* 构建源代码分发包：sdist命令将软件包的源代码及其相关文件打包成一个压缩文件（通常是.tar.gz或.zip格式）。该分发包可以用于在其他环境中安装和使用软件包。

* 生成MANIFEST.in文件：MANIFEST.in文件是一个可选的文件，用于指定哪些其他文件应该包含在源代码分发包中。sdist命令会根据MANIFEST.in文件的内容将这些文件包含在分发包中。

使用python setup.py sdist命令构建源代码分发包之前，你需要确保在当前目录下存在一个setup.py文件，并且该文件正确配置了软件包的元数据和依赖关系。

构建成功后，你将在当前目录下生成一个源代码分发包文件。你可以将该文件分享给其他人，或者使用pip命令将其安装到其他Python环境中。