# Super IDE Core Commands Guide

## 关键的Commands

* si project - Project Manager CLI

  > si project init/si init - Initialize a new Super IDE based project or update existing with new data. 

#### 用命令行创建一个新项目是怎样实现的？
```
# 通过一个指定的仓库作为examplecode创建一个新项目，或者通过mkdir example-proj && cd example-proj 创建一个空项目
git clone https://gitee.com/SuperIDE/examplecode.git --depth 1 ~/example-proj && cd ~/example-proj && rm -rf .git 

# 在项目目录下初始化项目
si project init

# 使用--project-dir或-d指定项目目录初始化项目
si project init -d ~/example-proj/

# -e或--environment ENV_NAME指定环境名称，同时-O或--project-option “key=value”指定env_image=环境镜像
si project init -d ~/example-proj/ -e test_env -O "env_image=denkchan/demo"
```

#### 克隆一个老项目

git clone https://gitee.com/SuperIDE/examplecode.git # 项目名称即为仓库名称

* si run - Run project targets over environments declared in “platformio.ini” (Project Configuration File).


### 编译构建项目的方法

```
si run
```

* si home - Launch Super IDE Home Web-server.

### 如何调试项目？

* si debug - Prepare Super IDE project for debugging or launch debug server.

### 烧录/标定和测试

todo

