## Super IDE Core Design

- setup.py 用于构建 Python 软件包，setup.py 文件中的配置信息来定义软件包的元数据和依赖关系。
- superide/**init**.py 定义一些常量字符串
- superide/**main**.py 入口代码
- superide/cli.py - class SuperIDECLI(click.MultiCommand)负责在 superide 的子目录下找 self.\_root_path.rglob("cli.py")，二级命令在 cli.py 中通过 click.group 来定义。

### superide/platform

PlatformBase 用于管理 Platform 所支持的 Board 即\_BOARDS_CACHE = {}

```
class PlatformBase(
    PlatformPackagesMixin, PlatformRunMixin
)
class PlatformBoardConfig
class PlatformFactory # Platform工厂用于创建PlatformBase对象
```

### superide/project

- superide/project/config.py 用于管理项目配置文件 SuperIDE.ini

### superide/home/rpc/handlers
