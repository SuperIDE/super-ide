Super IDE Core
===============

A professional Cross-platform IDE. 
Cross-platform IDE and Unified Debugger. 
Static Code Analyzer and Remote Unit Testing. 
Multi-platform and Multi-architecture Build System. 
Firmware File Explorer and Memory Inspection. 
IoT, Arduino, CMSIS, ESP-IDF, FreeRTOS, libOpenCM3, mbedOS, Pulp OS, SPL, 
STM32Cube, Zephyr RTOS, ARM, AVR, Espressif (ESP8266/ESP32), FPGA, 
MCS-51 (8051), MSP430, Nordic (nRF51/nRF52), NXP i.MX RT, PIC32, RISC-V, 
STMicroelectronics (STM8/STM32), Teensy


Get Started
-----------

* `Super IDE Core Get Started <https://gitee.com/SuperIDE/super-ide/blob/master/README.md>`_

License
-------

Copyright (c) 2022-present Mengning <contact@mengning.com.cn>

The Super IDE is licensed under the AGPL 3.0 license or commercial license.