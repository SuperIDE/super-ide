# Copyright (c) Mengning Software. 2023. All rights reserved.
#
# Super IDE licensed under GNU Affero General Public License v3 (AGPL-3.0) .
# You can use this software according to the terms and conditions of the AGPL-3.0.
# You may obtain a copy of AGPL-3.0 at:
#
#    https://www.gnu.org/licenses/agpl-3.0.txt
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the AGPL-3.0 for more details.

# pylint: disable=unused-argument

import pytest

from superide.package.commands.exec import package_exec_cmd
from superide.util import strip_ansi_codes


def test_pkg_not_installed(clirunner, validate_cliresult, isolated_pio_core):
    result = clirunner.invoke(
        package_exec_cmd,
        ["--", "openocd"],
    )
    with pytest.raises(
        AssertionError,
        match=("Could not find a package with 'openocd' executable file"),
    ):
        validate_cliresult(result)


def test_pkg_specified(clirunner, validate_cliresult, isolated_pio_core):
    # with install
    result = clirunner.invoke(
        package_exec_cmd,
        ["-p", "superide/tool-openocd", "--", "openocd", "--version"],
        obj=dict(force_click_stream=True),
    )
    validate_cliresult(result)
    output = strip_ansi_codes(result.output)
    assert "Tool Manager: Installing superide/tool-openocd" in output
    assert "Open On-Chip Debugger" in output


def test_unrecognized_options(clirunner, validate_cliresult, isolated_pio_core):
    # unrecognized option
    result = clirunner.invoke(
        package_exec_cmd,
        ["--", "openocd", "--test-unrecognized"],
        obj=dict(force_click_stream=True),
    )
    with pytest.raises(
        AssertionError,
        match=(r"openocd: (unrecognized|unknown) option"),
    ):
        validate_cliresult(result)
