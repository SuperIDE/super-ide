# Copyright (c) Mengning Software. 2023. All rights reserved.
#
# Super IDE licensed under GNU Affero General Public License v3 (AGPL-3.0) .
# You can use this software according to the terms and conditions of the AGPL-3.0.
# You may obtain a copy of AGPL-3.0 at:
#
#    https://www.gnu.org/licenses/agpl-3.0.txt
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the AGPL-3.0 for more details.

# pylint: disable=unused-argument

import re

from superide import __configfile__
from superide.package.commands.install import package_install_cmd
from superide.package.commands.outdated import package_outdated_cmd

PROJECT_OUTDATED_CONFIG_TPL = """
[env:devkit]
platform = superide/atmelavr@^2
framework = arduino
board = attiny88
lib_deps = milesburton/DallasTemperature@~3.8.0
"""

PROJECT_UPDATED_CONFIG_TPL = """
[env:devkit]
platform = superide/atmelavr@<4
framework = arduino
board = attiny88
lib_deps = milesburton/DallasTemperature@^3.8.0
"""


def test_project(clirunner, validate_cliresult, isolated_pio_core, tmp_path):
    project_dir = tmp_path / "project"
    project_dir.mkdir()
    (project_dir / __configfile__).write_text(PROJECT_OUTDATED_CONFIG_TPL)
    result = clirunner.invoke(package_install_cmd, ["-d", str(project_dir)])
    validate_cliresult(result)

    # overwrite config
    (project_dir / __configfile__).write_text(PROJECT_UPDATED_CONFIG_TPL)
    result = clirunner.invoke(package_outdated_cmd, ["-d", str(project_dir)])
    validate_cliresult(result)

    # validate output
    assert "Checking" in result.output
    assert re.search(
        r"^atmelavr\s+2\.2\.0\s+3\.\d+\.\d+\s+[456789]\.\d+\.\d+\s+Platform\s+devkit",
        result.output,
        re.MULTILINE,
    )
    assert re.search(
        r"^DallasTemperature\s+3\.8\.1\s+3\.\d+\.\d+\s+3\.\d+\.\d+\s+Library\s+devkit",
        result.output,
        re.MULTILINE,
    )
