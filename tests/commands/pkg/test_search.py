# Copyright (c) Mengning Software. 2023. All rights reserved.
#
# Super IDE licensed under GNU Affero General Public License v3 (AGPL-3.0) .
# You can use this software according to the terms and conditions of the AGPL-3.0.
# You may obtain a copy of AGPL-3.0 at:
#
#    https://www.gnu.org/licenses/agpl-3.0.txt
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the AGPL-3.0 for more details.

from superide.package.commands.search import package_search_cmd


def test_empty_query(clirunner, validate_cliresult):
    result = clirunner.invoke(
        package_search_cmd,
        [""],
    )
    validate_cliresult(result)
    assert all(t in result.output for t in ("Found", "Official", "page 1 of"))


def test_pagination(clirunner, validate_cliresult):
    result = clirunner.invoke(
        package_search_cmd,
        ["type:tool"],
    )
    validate_cliresult(result)
    assert all(t in result.output for t in ("Verified Tool", "page 1 of"))

    result = clirunner.invoke(
        package_search_cmd,
        ["type:tool", "-p", "10"],
    )
    validate_cliresult(result)
    assert all(t in result.output for t in ("Tool", "page 10 of"))


def test_sorting(clirunner, validate_cliresult):
    result = clirunner.invoke(
        package_search_cmd,
        ["OneWire", "-s", "popularity"],
    )
    validate_cliresult(result)
    assert "paulstoffregen/OneWire" in result.output


def test_not_found(clirunner, validate_cliresult):
    result = clirunner.invoke(
        package_search_cmd,
        ["name:unknown-package"],
    )
    validate_cliresult(result)
    assert "Nothing has been found" in result.output
