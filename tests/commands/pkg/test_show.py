# Copyright (c) Mengning Software. 2023. All rights reserved.
#
# Super IDE licensed under GNU Affero General Public License v3 (AGPL-3.0) .
# You can use this software according to the terms and conditions of the AGPL-3.0.
# You may obtain a copy of AGPL-3.0 at:
#
#    https://www.gnu.org/licenses/agpl-3.0.txt
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the AGPL-3.0 for more details.

import pytest

from superide.exception import UserSideException
from superide.package.commands.show import package_show_cmd


def test_spec_name(clirunner, validate_cliresult):
    # library
    result = clirunner.invoke(
        package_show_cmd,
        ["ArduinoJSON"],
    )
    validate_cliresult(result)
    assert "bblanchon/ArduinoJson" in result.output
    assert "Library" in result.output

    # platform
    result = clirunner.invoke(
        package_show_cmd,
        ["espressif32"],
    )
    validate_cliresult(result)
    assert "superide/espressif32" in result.output
    assert "Platform" in result.output

    # tool
    result = clirunner.invoke(
        package_show_cmd,
        ["tool-jlink"],
    )
    validate_cliresult(result)
    assert "superide/tool-jlink" in result.output
    assert "tool" in result.output


def test_spec_owner(clirunner, validate_cliresult):
    result = clirunner.invoke(
        package_show_cmd,
        ["bblanchon/ArduinoJSON"],
    )
    validate_cliresult(result)
    assert "bblanchon/ArduinoJson" in result.output
    assert "Library" in result.output

    # test broken owner
    result = clirunner.invoke(
        package_show_cmd,
        ["unknown/espressif32"],
    )
    with pytest.raises(UserSideException, match="Could not find"):
        raise result.exception


def test_complete_spec(clirunner, validate_cliresult):
    result = clirunner.invoke(
        package_show_cmd,
        ["bblanchon/ArduinoJSON", "-t", "library"],
    )
    validate_cliresult(result)
    assert "bblanchon/ArduinoJson" in result.output
    assert "Library" in result.output

    # tool
    result = clirunner.invoke(
        package_show_cmd,
        ["superide/tool-jlink", "-t", "tool"],
    )
    validate_cliresult(result)
    assert "superide/tool-jlink" in result.output
    assert "tool" in result.output


def test_name_conflict(clirunner):
    result = clirunner.invoke(
        package_show_cmd,
        ["OneWire", "-t", "library"],
    )
    assert "More than one package" in result.output
    assert isinstance(result.exception, UserSideException)


def test_spec_version(clirunner, validate_cliresult):
    result = clirunner.invoke(
        package_show_cmd,
        ["bblanchon/ArduinoJSON@5.13.4"],
    )
    validate_cliresult(result)
    assert "bblanchon/ArduinoJson" in result.output
    assert "Library • 5.13.4" in result.output
