# Copyright (c) Mengning Software. 2023. All rights reserved.
#
# Super IDE licensed under GNU Affero General Public License v3 (AGPL-3.0) .
# You can use this software according to the terms and conditions of the AGPL-3.0.
# You may obtain a copy of AGPL-3.0 at:
#
#    https://www.gnu.org/licenses/agpl-3.0.txt
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the AGPL-3.0 for more details.

from superide import __configfile__
from superide.package.meta import PackageSpec
from superide.project.config import ProjectConfig
from superide.project.savedeps import save_project_dependencies

PROJECT_CONFIG_TPL = """
[env]
board = uno
framework = arduino
lib_deps =
    SPI
platform_packages =
    superide/tool-jlink@^1.75001.0

[env:bare]

[env:release]
platform = superide/espressif32
lib_deps =
    milesburton/DallasTemperature@^3.8

[env:debug]
platform = superide/espressif32@^3.4.0
lib_deps =
    ${env.lib_deps}
    milesburton/DallasTemperature@^3.9.1
    bblanchon/ArduinoJson
platform_packages =
    ${env.platform_packages}
    superide/framework-arduinoespressif32 @ https://github.com/espressif/arduino-esp32.git
"""


def test_save_libraries(tmp_path):
    project_dir = tmp_path / "project"
    project_dir.mkdir()
    (project_dir / __configfile__).write_text(PROJECT_CONFIG_TPL)
    specs = [
        PackageSpec("milesburton/DallasTemperature@^3.9"),
        PackageSpec("adafruit/Adafruit GPS Library@^1.6.0"),
        PackageSpec("https://github.com/nanopb/nanopb.git"),
    ]

    # add to the specified environment
    save_project_dependencies(
        str(project_dir), specs, scope="lib_deps", action="add", environments=["debug"]
    )
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:debug", "lib_deps") == [
        "SPI",
        "bblanchon/ArduinoJson",
        "milesburton/DallasTemperature@^3.9",
        "adafruit/Adafruit GPS Library@^1.6.0",
        "https://github.com/nanopb/nanopb.git",
    ]
    assert config.get("env:bare", "lib_deps") == ["SPI"]
    assert config.get("env:release", "lib_deps") == [
        "milesburton/DallasTemperature@^3.8"
    ]

    # add to the the all environments
    save_project_dependencies(str(project_dir), specs, scope="lib_deps", action="add")
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:debug", "lib_deps") == [
        "SPI",
        "bblanchon/ArduinoJson",
        "milesburton/DallasTemperature@^3.9",
        "adafruit/Adafruit GPS Library@^1.6.0",
        "https://github.com/nanopb/nanopb.git",
    ]
    assert config.get("env:bare", "lib_deps") == [
        "milesburton/DallasTemperature@^3.9",
        "adafruit/Adafruit GPS Library@^1.6.0",
        "https://github.com/nanopb/nanopb.git",
    ]
    assert config.get("env:release", "lib_deps") == [
        "milesburton/DallasTemperature@^3.9",
        "adafruit/Adafruit GPS Library@^1.6.0",
        "https://github.com/nanopb/nanopb.git",
    ]

    # remove deps from env
    save_project_dependencies(
        str(project_dir),
        [PackageSpec("milesburton/DallasTemperature")],
        scope="lib_deps",
        action="remove",
        environments=["release"],
    )
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:release", "lib_deps") == [
        "adafruit/Adafruit GPS Library@^1.6.0",
        "https://github.com/nanopb/nanopb.git",
    ]
    # invalid requirements
    save_project_dependencies(
        str(project_dir),
        [PackageSpec("adafruit/Adafruit GPS Library@^9.9.9")],
        scope="lib_deps",
        action="remove",
        environments=["release"],
    )
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:release", "lib_deps") == [
        "https://github.com/nanopb/nanopb.git",
    ]

    # remove deps from all envs
    save_project_dependencies(
        str(project_dir), specs, scope="lib_deps", action="remove"
    )
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:debug", "lib_deps") == [
        "SPI",
        "bblanchon/ArduinoJson",
    ]
    assert config.get("env:bare", "lib_deps") == ["SPI"]
    assert config.get("env:release", "lib_deps") == ["SPI"]


def test_save_tools(tmp_path):
    project_dir = tmp_path / "project"
    project_dir.mkdir()
    (project_dir / __configfile__).write_text(PROJECT_CONFIG_TPL)
    specs = [
        PackageSpec("superide/framework-espidf@^2"),
        PackageSpec("superide/tool-esptoolpy"),
    ]

    # add to the specified environment
    save_project_dependencies(
        str(project_dir),
        specs,
        scope="platform_packages",
        action="add",
        environments=["debug"],
    )
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:debug", "platform_packages") == [
        "superide/tool-jlink@^1.75001.0",
        "superide/framework-arduinoespressif32 @ https://github.com/espressif/arduino-esp32.git",
        "superide/framework-espidf@^2",
        "superide/tool-esptoolpy",
    ]
    assert config.get("env:bare", "platform_packages") == [
        "superide/tool-jlink@^1.75001.0"
    ]
    assert config.get("env:release", "platform_packages") == [
        "superide/tool-jlink@^1.75001.0"
    ]

    # add to the the all environments
    save_project_dependencies(
        str(project_dir), specs, scope="platform_packages", action="add"
    )
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:debug", "platform_packages") == [
        "superide/tool-jlink@^1.75001.0",
        "superide/framework-arduinoespressif32 @ https://github.com/espressif/arduino-esp32.git",
        "superide/framework-espidf@^2",
        "superide/tool-esptoolpy",
    ]
    assert config.get("env:bare", "platform_packages") == [
        "superide/framework-espidf@^2",
        "superide/tool-esptoolpy",
    ]
    assert config.get("env:release", "platform_packages") == [
        "superide/framework-espidf@^2",
        "superide/tool-esptoolpy",
    ]

    # remove deps from env
    save_project_dependencies(
        str(project_dir),
        [PackageSpec("superide/framework-espidf")],
        scope="platform_packages",
        action="remove",
        environments=["release"],
    )
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:release", "platform_packages") == [
        "superide/tool-esptoolpy",
    ]
    # invalid requirements
    save_project_dependencies(
        str(project_dir),
        [PackageSpec("superide/tool-esptoolpy@9.9.9")],
        scope="platform_packages",
        action="remove",
        environments=["release"],
    )
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:release", "platform_packages") == [
        "superide/tool-jlink@^1.75001.0",
    ]

    # remove deps from all envs
    save_project_dependencies(
        str(project_dir), specs, scope="platform_packages", action="remove"
    )
    config = ProjectConfig.get_instance(str(project_dir / __configfile__))
    assert config.get("env:debug", "platform_packages") == [
        "superide/tool-jlink@^1.75001.0",
        "superide/framework-arduinoespressif32 @ https://github.com/espressif/arduino-esp32.git",
    ]
    assert config.get("env:bare", "platform_packages") == [
        "superide/tool-jlink@^1.75001.0"
    ]
    assert config.get("env:release", "platform_packages") == [
        "superide/tool-jlink@^1.75001.0"
    ]
