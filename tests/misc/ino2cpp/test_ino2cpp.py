# Copyright (c) Mengning Software. 2023. All rights reserved.
#
# Super IDE licensed under GNU Affero General Public License v3 (AGPL-3.0) .
# You can use this software according to the terms and conditions of the AGPL-3.0.
# You may obtain a copy of AGPL-3.0 at:
#
#    https://www.gnu.org/licenses/agpl-3.0.txt
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the AGPL-3.0 for more details.

from os import listdir
from os.path import dirname, isdir, join, normpath

from superide.legacycode.ci import cli as cmd_ci

EXAMPLES_DIR = normpath(join(dirname(__file__), "examples"))


def pytest_generate_tests(metafunc):
    if "piotest_dir" not in metafunc.fixturenames:
        return
    test_dirs = []
    for name in listdir(EXAMPLES_DIR):
        if isdir(join(EXAMPLES_DIR, name)):
            test_dirs.append(join(EXAMPLES_DIR, name))
    test_dirs.sort()
    metafunc.parametrize("piotest_dir", test_dirs)


def test_example(clirunner, validate_cliresult, piotest_dir):
    result = clirunner.invoke(cmd_ci, [piotest_dir, "-b", "uno"])
    validate_cliresult(result)


def test_warning_line(clirunner, validate_cliresult):
    result = clirunner.invoke(cmd_ci, [join(EXAMPLES_DIR, "basic"), "-b", "uno"])
    validate_cliresult(result)
    assert 'basic.ino:16:14: warning: #warning "Line number is 16"' in result.output
    assert 'basic.ino:46:2: warning: #warning "Line number is 46"' in result.output
    result = clirunner.invoke(
        cmd_ci, [join(EXAMPLES_DIR, "strmultilines"), "-b", "uno"]
    )
    validate_cliresult(result)
    assert 'main.ino:75:2: warning: #warning "Line 75"' in result.output
