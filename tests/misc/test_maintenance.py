# Copyright (c) Mengning Software. 2023. All rights reserved.
#
# Super IDE licensed under GNU Affero General Public License v3 (AGPL-3.0) .
# You can use this software according to the terms and conditions of the AGPL-3.0.
# You may obtain a copy of AGPL-3.0 at:
#
#    https://www.gnu.org/licenses/agpl-3.0.txt
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the AGPL-3.0 for more details.

# pylint: disable=unused-argument

from time import time

from superide import app, maintenance
from superide.__main__ import cli as cli_pio
from superide.legacycode import upgrade as cmd_upgrade


def test_check_pio_upgrade(clirunner, isolated_pio_core, validate_cliresult):
    def _patch_pio_version(version):
        maintenance.__version__ = version
        cmd_upgrade.VERSION = version.split(".", 3)

    interval = int(app.get_setting("check_superide_interval")) * 3600 * 24
    last_check = {"platformio_upgrade": time() - interval - 1}
    origin_version = maintenance.__version__

    # check development version
    _patch_pio_version("3.0.0-a1")
    app.set_state_item("last_check", last_check)
    result = clirunner.invoke(cli_pio, ["platform", "list"])
    validate_cliresult(result)
    assert "There is a new version" in result.output
    assert "Please upgrade" in result.output

    # check stable version
    _patch_pio_version("2.11.0")
    app.set_state_item("last_check", last_check)
    result = clirunner.invoke(cli_pio, ["platform", "list"])
    validate_cliresult(result)
    assert "There is a new version" in result.output
    assert "Please upgrade" in result.output

    # restore original version
    _patch_pio_version(origin_version)
