# Copyright (c) Mengning Software. 2023. All rights reserved.
#
# Super IDE licensed under GNU Affero General Public License v3 (AGPL-3.0) .
# You can use this software according to the terms and conditions of the AGPL-3.0.
# You may obtain a copy of AGPL-3.0 at:
#
#    https://www.gnu.org/licenses/agpl-3.0.txt
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the AGPL-3.0 for more details.

# pylint: disable=unused-argument

import pytest
import requests

from superide import __check_internet_hosts__, http, proc
from superide.registry.client import RegistryClient


def test_platformio_cli():
    result = proc.exec_command(["pio", "--help"])
    assert result["returncode"] == 0
    # pylint: disable=unsupported-membership-test
    assert "Usage: pio [OPTIONS] COMMAND [ARGS]..." in result["out"]


def test_ping_internet_ips():
    for host in __check_internet_hosts__:
        requests.get("http://%s" % host, allow_redirects=False, timeout=2)


def test_api_internet_offline(without_internet, isolated_pio_core):
    regclient = RegistryClient()
    with pytest.raises(http.InternetConnectionError):
        regclient.fetch_json_data("get", "/v3/search")


def test_api_cache(monkeypatch, isolated_pio_core):
    regclient = RegistryClient()
    api_kwargs = {"method": "get", "path": "/v3/search", "x_cache_valid": "10s"}
    result = regclient.fetch_json_data(**api_kwargs)
    assert result and "total" in result
    monkeypatch.setattr(http, "_internet_on", lambda: False)
    assert regclient.fetch_json_data(**api_kwargs) == result
