#!/bin/bash

set -x
set -e

# export LDFLAGS
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH
export LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib/x86_64-linux-gnu/:$LD_LIBRARY_PATH
echo "/usr/local/lib" >> /etc/ld.so.conf
apt install -y g++ libprotobuf-dev protobuf-compiler protobuf-compiler-grpc libgrpc++-dev libgrpc-dev libtool automake autoconf cmake make pkg-config libyajl-dev zlib1g-dev libselinux1-dev libseccomp-dev libcap-dev libsystemd-dev git libarchive-dev libcurl4-gnutls-dev openssl libdevmapper-dev python3 libtar0 libtar-dev libhttp-parser-dev libwebsockets-dev language-pack-en

BUILD_DIR=/tmp/build_isulad

rm -rf $BUILD_DIR
mkdir -p $BUILD_DIR

# build lxc
cd $BUILD_DIR
git clone -b openEuler-22.03-LTS-SP1 https://gitee.com/src-openeuler/lxc.git
cd lxc
git checkout d750e6fbd680b86ed4f992e32c680f35ba519d2b
git config --global --add safe.directory /tmp/build_isulad/lxc/lxc-4.0.3
./apply-patches
cd lxc-4.0.3
./autogen.sh
./configure --disable-werror
make -j $(nproc)
make install

# build lcr
cd $BUILD_DIR
git clone https://gitee.com/openeuler/lcr.git
cd lcr
git checkout 9a2c713c1037c36378b95378e42701af3b5bd8d2
mkdir build
cd build
cmake ..
make -j $(nproc)
make install

# build and install clibcni
cd $BUILD_DIR
git clone https://gitee.com/openeuler/clibcni.git
cd clibcni
git checkout 97c54727a85a7046fb93b9c0de5946b7c7c9fad7
mkdir build
cd build
cmake ..
make -j $(nproc)
make install

# build and install iSulad
cd $BUILD_DIR
git clone https://gitee.com/openeuler/iSulad.git
cd iSulad
git checkout 48dc6f0adda72d7f4742afe1b8380370debfe4f4
mkdir build
cd build
cmake ..
make -j $(nproc)
make install

# clean
# rm -rf $BUILD_DIR
apt autoremove
ldconfig

# 添加用户组
groupadd -g 991 isula

