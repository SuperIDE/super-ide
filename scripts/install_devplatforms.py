# Copyright (c) Mengning Software. 2023. All rights reserved.
#
# Super IDE licensed under GNU Affero General Public License v3 (AGPL-3.0) .
# You can use this software according to the terms and conditions of the AGPL-3.0.
# You may obtain a copy of AGPL-3.0 at:
#
#    https://www.gnu.org/licenses/agpl-3.0.txt
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the AGPL-3.0 for more details.

import json
import subprocess
import sys

import click


@click.command()
@click.option("--desktop", is_flag=True, default=False)
@click.option(
    "--names",
    envvar="PIO_INSTALL_DEVPLATFORM_NAMES",
    help="Install specified platform (split by comma)",
)
@click.option(
    "--ownernames",
    envvar="PIO_INSTALL_DEVPLATFORM_OWNERNAMES",
    help="Filter by ownernames (split by comma)",
)
def main(desktop, names, ownernames):
    platforms = json.loads(
        subprocess.check_output(["pio", "platform", "search", "--json-output"]).decode()
    )
    names = [n.strip() for n in (names or "").split(",") if n.strip()]
    ownernames = [n.strip() for n in (ownernames or "").split(",") if n.strip()]
    for platform in platforms:
        skip = [
            not desktop and platform["forDesktop"],
            names and platform["name"] not in names,
            ownernames and platform["ownername"] not in ownernames,
        ]
        if any(skip):
            continue
        subprocess.check_call(
            [
                "pio",
                "pkg",
                "install",
                "--global",
                "--skip-dependencies",
                "--platform",
                "{ownername}/{name}".format(**platform),
            ]
        )


if __name__ == "__main__":
    sys.exit(main())
