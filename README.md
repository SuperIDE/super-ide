# Super IDE Core

## 用户指南

您安装 Super IDE VSCode插件时会自动安装 Super IDE Core，这里简单介绍一下手动安装 Super IDE Core。

- pip 安装 Super IDE Core

```
$ python3 -m pip install -U super-ide
```

- 推荐使用get-superide.py脚本安装 Super IDE Core，因为这种方式和Super IDE VSCode插件采用了相同的方法。

```
$ git clone https://gitee.com/SuperIDE/super-ide.git 
$ cd super-ide/
$ python3 scripts/get-superide.py
```

### Run

您使用 Super IDE VSCode插件时内部会调用 Super IDE Core，这里简单介绍一下单独命令行方式使用 Super IDE Core：使用`super-ide`或`si`命令

```bash
super-ide --version
si --version
```

* [Super IDE Core Commands Guide](doc/CommandsGuide.md)

## 开发者指南

### 环境

Windows系统下使用[WSL](https://learn.microsoft.com/zh-cn/windows/wsl/)，Mac和Linux下可以直接运行在终端。推荐使用Conda环境。

本教程以WSL下Ubuntu系统的MiniConda环境安装使用为例。

#### WSL安装

如果没有**WSL**首先在Windows下安装[WSL下的Ubuntu](https://learn.microsoft.com/zh-cn/windows/wsl/install)。

#### Miniconda安装

下载[Miniconda](https://docs.conda.io/projects/miniconda/en/latest/)到Ubuntu，在此目录下运行：

```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

安装成功后刷新环境变量：

```bash
source .bashrc
```
执行完source .bashrc 之后，命令行提示信息前面就会有一个(base)，代表激活了base(最基础)环境。

```bash
conda deactivate		        #退出base环境
```

[参考](https://blog.51cto.com/u_13389043/6223883)

#### 准备Conda环境

创建side环境，后续在此环境下进行开发：

```bash
conda create --name side python=3	#创建名为side,python为最新3.x版本的环境
conda activate side		        #进入该环境
conda deactivate		        #退出该环境
```

### Install

下载本项目并进入项目目录：

```bash
git clone https://gitee.com/SuperIDE/super-ide.git 
cd super-ide/
```

- 源代码中开发者模式安装 Super IDE Core

  ```bash
  pip install -e .
  ```

  - `pip`会根据`setup.py`文件中的配置要求安装项目所需的依赖项，该选项将把项目安装在本目录下并在Python的site-packages目录中创建符号链接。


- 源代码中安装 Super IDE Core

  ```bash
  pip install .
  ```

  - `pip`会根据`setup.py`文件中的配置要求安装项目所需的依赖项，并将项目安装到Python的site-packages目录中。

​	验证安装：在命令行中输入`si`或`super-ide`，将看到帮助提示。

### Debug

按照之前步骤在conda虚拟环境采用开发者模式安装side后可以在vscode中进行调试：

在根目录下新建一个调试项目文件夹并在其中打开vscode：

```bash
(side) ~$ mkdir Debug
(side) ~$ cd Debug/
(side) ~/Debug$ code .
```

在此目录下创建.vscode文件夹，在其中新建`launch.json`文件

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Debug",
            "type": "python",
            "request": "launch",
            "python": "/home/username/miniconda3/envs/side/bin/python",//python解释器
            "program": "/home/username/miniconda3/envs/side/bin/si",//调试程序入口
            "console": "integratedTerminal",
            "args": [
                "project",
                "init",
                "--board",
                "disco_f746ng"
            ],//命令行参数
        }
    ]
}
```

其中`python`为python解释器位置，`program`为调试程序入口，可在终端查询：

```bash
(side) ~/Debug$ which python
/home/username/miniconda3/envs/side/bin/python
(side) ~/Debug$ which si
/home/username/miniconda3/envs/side/bin/si
```

`args`为待调试命令行参数，本例中即相当于在命令行运行`si project init --board disco_f746ng`

之后在vscode中将`super-ide/`中的代码文件在此窗口下打开即可进行添加断点等调试设置，在Debug文件夹下启动调试（或按F5）开始调试。

### Build

```
$ pip show setuptools
$ make pack
```

### Publish

```
$ make publish
```

运行命令之前需要将用户根目录下配置好 pypi 的 API Token。
格式如下：

```
[distutils]
index-servers =
    pypi
[pypi]
username = __token__
password = {api token}
```

### iSulad安装指南

side使用isulad容器引擎，对于开发者提供在wsl2下使用ubuntu20.04发行版安装isulad的一种方法步骤。

1. 为快速获取软件包，建议换为国内源，换源教程可参考:[Ubuntu20.04 更换镜像源步骤](https://blog.csdn.net/tangling1/article/details/132150597)

   更新软件包列表：

   ```bash
   sudo apt update
   ```

2. 执行脚本，脚本在`super-ide/scripts`目录下

   ```bash
   cd super-ide/scripts
   sudo chmod +x install_iSulad_on_Ubuntu_20_04_LTS.sh
   sudo ./install_iSulad_on_Ubuntu_20_04_LTS.sh
   ```

3. 加入用户组

   ```bash
   sudo usermod -aG isula username	#username更改为当前用户名
   ```

4. 添加docker镜像网站

   成功安装`iSulad`之后，需要先配置好容器镜像的注册地址，以"docker.io"为例：

   ```bash
   # cat /etc/isulad/daemon.json
   .....
       "registry-mirrors": [
           "docker.io"
       ],
   .....
   ```

5. 打开isulad守护进程

   ```bash
   sudo isulad
   ```

   出现`iSulad successfully booted in 0.xxx s`则启动成功

6. 另外打开一个终端使用isula，原终端可作为isulad日志查看

   ```bash
   isula ps -a
   ```

   出现`CONTAINER ID    IMAGE   COMMAND CREATED STATUS  PORTS   NAMES`则功能正常

7. 使用isula，功能可参考[官方文档](https://docs.openeuler.org/zh/docs/22.03_LTS/docs/Container/%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97.html)


## Super IDE Core Inside

- [make pack](doc/makepack.md)
- [Super IDE Core Design](doc/CoreDesign.md)

## License

Copyright (c) 2022-present Mengning <mengning997@163.com>

The Super IDE is licensed under the AGPL 3.0 license or commercial license.
