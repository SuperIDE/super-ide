lint:
	pylint --rcfile=./.pylintrc ./tests
	pylint --rcfile=./.pylintrc ./superide

isort:
	isort ./superide
	isort ./tests

format:
	black ./superide
	black ./tests

test:
	python3 superide/registry/cli.py # pull environment image
	# py.test --verbose --exitfirst -n 6 --dist=loadscope tests --ignore tests/test_examples.py

before-commit: isort format lint

clean-docs:
	rm -rf docs/_build

clean: clean-docs
	find . -name \*.pyc -delete
	find . -name __pycache__ -delete
	rm -rf .cache
	rm -rf build
	rm -rf htmlcov
	rm -f .coverage

profile:
	# Usage $ > make PIOARGS="boards" profile
	python -m cProfile -o .tox/.tmp/cprofile.prof -m superide ${PIOARGS}
	snakeviz .tox/.tmp/cprofile.prof

pack:
	python3 setup.py sdist

publish:
	rm -rf dist
	python3 setup.py sdist 
	python3 -m twine upload dist/*
